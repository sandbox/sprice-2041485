<?php
/**
 * @file
 * Extends Term Permissions with rules actions to add and remove permissions
 * from users and roles.
 */

/**
 * Invokes hook_rules_action_info() in order to create our actions.
 */
function term_permissions_rules_actions_rules_action_info() {

  $actions = array(
    'term_permissions_rules_actions_add_user' => array(
      'label' => t('Grant a term permission to a user'),
      'group' => t('Taxonomy Term Permissions'),
      'parameter' => array(
        'user' => array(
          'type' => 'text',
          'label' => t('User (uid)'),
        ),
        'term' => array(
          'type' => 'text',
          'label' => t('Term (tid)'),
        ),
      ),
    ),
    'term_permissions_rules_actions_remove_user' => array(
      'label' => t('Revoke a term permission from a user'),
      'group' => t('Taxonomy Term Permissions'),
      'parameter' => array(
        'user' => array(
          'type' => 'text',
          'label' => t('User (uid)'),
        ),
        'term' => array(
          'type' => 'text',
          'label' => t('Term (tid)'),
        ),
      ),
    ),
    'term_permissions_rules_actions_add_role' => array(
      'label' => t('Grant a term permission to a role'),
      'group' => t('Taxonomy Term Permissions'),
      'parameter' => array(
        'role' => array(
          'type' => 'text',
          'label' => t('Role (rid)'),
        ),
        'term' => array(
          'type' => 'text',
          'label' => t('Term (tid)'),
        ),
      ),
    ),
    'term_permissions_rules_actions_remove_role' => array(
      'label' => t('Revoke a term permission from a role'),
      'group' => t('Taxonomy Term Permissions'),
      'parameter' => array(
        'role' => array(
          'type' => 'text',
          'label' => t('Role (rid)'),
        ),
        'term' => array(
          'type' => 'text',
          'label' => t('Term (tid)'),
        ),
      ),
    ),
  );
  return $actions;
}

/**
 * Adds a user permission for a single term.
 *
 * @param text $user
 *   The user id to set the permission for.
 * @param text $term
 *   The term id to set the permission for.
 */
function term_permissions_rules_actions_add_user($user, $term) {

  if (!(term_permissions_rules_actions_check_existence($user, 'uid'))) {
    // Check to see if the user exists.
    drupal_set_message(t('No user exists with that uid (@user).',
      array('@user' => $user)), 'error');
  }
  elseif (!(term_permissions_rules_actions_check_existence($term, 'tid'))) {
    // Check to see if the term exists.
    drupal_set_message(t('No term exists with that tid (@term).',
      array('@term' => $term)), 'error');
  }
  elseif (term_permissions_rules_actions_check_perms($term, $user, '')) {
    // Both arguments exist; see if the permission is already in the table.
    drupal_set_message(t('User already has permission set. Nothing changed.'),
      'warning');
  }
  else {
    // Insert the value into the table.
    db_insert('term_permissions_user')->fields(array(
        'tid' => $term,
        'uid' => $user,
    ))->execute();

    // Set success status.
    drupal_set_message(t('Permission added to uid (@user)',
      array('@user' => $user)), 'status');
  }
}

/**
 * Removes a user permission for a single term.
 *
 * @param text $user
 *   The user id to set the permission for.
 * @param text $term
 *   The term id to set the permission for. 
 */
function term_permissions_rules_actions_remove_user($user, $term) {

  if (!(term_permissions_rules_actions_check_existence($user, 'uid'))) {
    // Check to see if the user exists.
    drupal_set_message(t('No user exists with that uid (@user).',
      array('@user' => $user)), 'error');
  }
  elseif (!(term_permissions_rules_actions_check_existence($term, 'tid'))) {
    // Check to see if the term exists.
    drupal_set_message(t('No term exists with that tid (@term).',
      array('@term' => $term)), 'error');
  }
  elseif (term_permissions_rules_actions_check_perms($term, $user, '')) {
    // Both arguments exist; see if the permission is already in the table.
    drupal_set_message(t('User already has permission set. Nothing changed.'),
      'warning');
  }
  else {
    // Remove the value from the table.
    db_delete('term_permissions_user')
      ->condition('tid', $term)
      ->condition('uid', $user)
      ->execute();

    // Set success status.
    drupal_set_message(t('Permission removed from uid (@user)',
      array('@user' => $user)), 'status');
  }
}

/**
 * Adds a role permission for a single term.
 *
 * @param text $role
 *   The role id to set the permission for.
 * @param text $term
 *   The term id to set the permission for.
 */
function term_permissions_rules_actions_add_role($role, $term) {

  if (!(term_permissions_rules_actions_check_existence($role, 'rid'))) {
    // Check to see if the role exists.
    drupal_set_message(t('No role exists with that rid (@role).',
      array('@role' => $role)), 'error');
  }
  elseif (!(term_permissions_rules_actions_check_existence($term, 'tid'))) {
    // Check to see if the term exists.
    drupal_set_message(t('No term exists with that tid (@term)',
      array('@term' => $term)), 'error');
  }
  elseif (term_permissions_rules_actions_check_perms($term, '', $role)) {
    // Both arguments exist; see if the permission is already in the table.
    drupal_set_message(t('Role already has permission set. Nothing changed'),
      'warning');
  }
  else {
    // Insert the values into the table.
    db_insert('term_permissions_role')
        ->fields(array(
          'tid' => $term,
          'rid' => $role,
        ))
        ->execute();

    // Set success status.
    drupal_set_message(t('Permission added to rid (@role).',
      array('@role' => $role)), 'status');
  }
}

/**
 * Removes a role permission for a single term.
 *
 * @param text $role
 *   The role id to set the permission for.
 * @param text $term
 *   The term id to set the permission for.
 */
function term_permissions_rules_actions_remove_role($role, $term) {

  if (!(term_permissions_rules_actions_check_existence($role, 'rid'))) {
    // Check to see if the role exists.
    drupal_set_message(t('No role exists with that rid (@role)',
      array('@role' => $role)), 'error');
  }
  elseif (!(term_permissions_rules_actions_check_existence($term, 'tid'))) {
    // Check to see if the term exists.
    drupal_set_message(t('No term exists with that tid (@term).',
      array('@term' => $term)), 'error');
  }
  elseif (term_permissions_rules_actions_check_perms($term, '', $role)) {
    // Both arguments exist; see if the permission is already in the table.
    drupal_set_message(t('Role already has permission set. Nothing changed.'),
      'warning');
  }
  else {
    // Remove the value from the table.
    db_delete('term_permissions_role')
      ->condition('tid', $term)
      ->condition('rid', $role)
      ->execute();

    // Set success status.
    drupal_set_message(t('Permission removed from rid (@role).',
      array('@role' => $role)), 'status');
  }
}

/**
 * Checks to see if a permission already exists in the table.
 *
 * @param text $term
 *   The term id associated with the permission.
 * @param text $user
 *   A user id associated with the permission.
 * @param text $role
 *   A role id associated with the permission.
 */
function term_permissions_rules_actions_check_perms($term, $user = '', $role = '') {

  // We need a tid.
  if (!isset($term)) {
    drupal_set_message(
      t('A term ID needs to be passed to set a permission.'), 'error');
    return FALSE;
  }

  // We need either a rid or a uid to be set; otherwise, we can't do anything.
  if ((!isset($user)) && (!isset($role))) {
    drupal_set_message(
      t('Either a role ID or a user ID needs to be passed to set a permission.'),
        'error');
    return FALSE;
  }

  // We need either a rid or a uid as not null; otherwise, we can't do anything.
  if ((is_null($user)) && (is_null($role))) {
    drupal_set_message(
      t('Either a role ID or a user ID needs to be passed to set a permission.'),
        'error');
    return FALSE;
  }

  // We can't have both an rid and a uid.
  if ((!empty($user)) && (!empty($role))) {
    drupal_set_message(t('Only one user or role needs to be passed.'), 'error');
    return FALSE;
  }

  if (!empty($user)) {
    $table = 'term_permissions_user';
    $type = 'uid';
    $obj = $user;
  }
  elseif (!empty($role)) {
    $table = 'term_permissions_role';
    $type = 'rid';
    $obj = $role;
  }

  $result = db_select($table, 't')
    ->fields('t', array($type))
    ->condition('tid', $term)
    ->condition($type, $obj)
    ->execute()
    ->rowCount();

  if ($result) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Checks to see if either a tid, uid, or rid exists.
 *
 * @param text $obj
 *   The value of the type to be checked for.
 * @param text $type
 *   Either tid, uid, or rid.
 */
function term_permissions_rules_actions_check_existence($obj, $type) {

  // Set the tablename; return false if not a tid, uid, or rid.
  switch ($type) {
    case 'tid':
      $table = 'taxonomy_term_data';
      break;

    case 'uid':
      $table = 'users';
      break;

    case 'rid':
      $table = 'role';
      break;

    default:
      drupal_set_message(t('Role not passed'), 'error');
      return FALSE;
  }

  // Run the query; if there are results, then it exists.
  // If not, then it doesn't.
  $result = db_select($table, 't')
    ->fields('t', array($type))
    ->condition($type, $obj)
    ->execute()
    ->rowCount();

  if ($result) {
    // The id exists.
    return TRUE;
  }
  else {
    // The id doesn't exist.
    return FALSE;
  }
  // We should never get here, but just in case, return false.
  return FALSE;
}
