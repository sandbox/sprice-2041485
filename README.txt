Term Permissions Rules Actions
---

This module extends the module Taxonomy Term Permissions by providing four rules

actions.

TTP creates access controls for individual taxonomy terms in that users can use

only the terms (per vocabulary) that they have permissions for. These

permissions are based either on an individual user or a user role, and the

permissions are set on the taxonomy term edit page itself.

This module provides four actions:

(1) Grant User a Term Permission

(2) Revoke a User's Term Permission

(3) Grant a Role a Term Permission

(4) Revoke a Role's Term Permission

Dependencies:
---

* Taxonomy Term Permissions

* Rules

Install:
---

(1) Install just like every module ever

(2) Make sure to flush the caches after installation so the actions appear in

Rules.

Credits

---

Developed and maintained by Shawn Patrick Rice.
